var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var mysql = require('mysql');
var fs = require('fs');

app.use(express.static('public'));

app.use(bodyParser.json()); // for parsing application/json

var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

var connect = function() {
  return mysql.createConnection(
    config.database
  );
};

app.get('/api/:state', function(req, res) {
  // console.info(req.params.state);
  // console.info(req.query.service);
  connection = connect();
  connection.connect();
  var services = "";
  var service = (typeof req.query.service !== 'string') ? req.query.service : [req.query.service + ""];
  service.forEach(function(ser) {
    services += ' and search_table.' + ser + '=1 ';
  });
  connection.query('select advertisers.* from advertisers, search_table where search_table.visible=1 and advertisers.advertiser_id=search_table.advertiser_id and search_table.state=? ' + services + ' limit 5', [req.params.state], function(err, rows, fields) {
    if (!err) {
      // console.info(rows);
      res.send(rows);
    } else {
      console.info(err);
    }
    connection.end();
  });
  //res.sendStatus(200);
});


app.listen(config.port, function() {
  console.log('server running on port: ' + this.address().port);
});
