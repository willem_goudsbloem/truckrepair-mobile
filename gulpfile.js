var gulp = require('gulp');

//clean public
var rimraf = require('gulp-rimraf');
gulp.task('clean', function() {
  return gulp.src('./public/**/*.*', {
      read: false
    })
    .pipe(rimraf());
});

// copy json files to public
gulp.task('copy-json', function() {
  gulp.src('./src/**/*.json')
    .pipe(gulp.dest('public/'));
});

// copy src without minify, etc for dev purposes
gulp.task('copy-src', function() {
  gulp.src('./src/**/*.*')
    .pipe(gulp.dest('public/'));
});

// compress and copy html files
var htmlmin = require('gulp-htmlmin');
gulp.task('minify-html', function() {
  return gulp.src('src/*.html')
    .pipe(htmlmin({
      collapseWhitespace: true
    }))
    .pipe(gulp.dest('public/'));
});

// copy and compress javascript files
var uglify = require('gulp-uglify');
gulp.task('uglify', function() {
  return gulp.src('src/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('public'));
});

var minifyCss = require('gulp-minify-css');
gulp.task('minify-css', function() {
  return gulp.src('src/**/*.css')
    .pipe(gulp.dest('public'));
});

gulp.task('watch', function() {
  gulp.watch('./src/**/*.*', ['copy-src']);
});

gulp.task('default', ['clean', 'copy-json', 'minify-html', 'uglify', 'minify-css'], function() {});
