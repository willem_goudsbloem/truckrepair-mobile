$(function() {
  //load states
  $.get('state.json', function(data) {
    data.forEach(function(d) {
      $('select#state').append('<option value=\"' + d.key + '">' + d.value + '</option>');
    });
  });
  //load services
  $.get('service.json', function(data) {
    data.forEach(function(d) {
      $('select#service').append('<option value=\"' + d.key + '">' + d.value + '</option>');
    });
    $("select#service").selectmenu("refresh");
  });
  //state select changed
  $('select#state').change(function(evt) {
    if (this.value !== "") {
      $('div#service_').show();
    } else {
      $('div#service_').hide();
    }
  });

  $('select#service').change(function(evt) {
    if (this.value.length > 0) {
      var ser="?";
      var i = 0;
      var services = (typeof $('select#service').val() === 'string') ? [$('select#service').val()] : $('select#service').val();
      services.forEach(function(ser0){
        if(i++>0) ser+= "&";
        ser += "service="+ser0;
      });
      $.get('/api/'+$('select#state').val()+ser, function(data) {
        res = '<ul data-role="listview" class="ui-listview">';
        data.forEach(function(d) {
          res += '<li class="ui-li ui-li-static ui-body-c">' + '<h3 class="ui-li-heading">' + d.company_name + '</h3>' + '<p class="ui-li-desc"><strong>' + d.city + '</strong></p>' + '<p class="ui-li-desc">' + d.address + '</p>' + '</li>';
        });
        res += '</ul>';
        $('div#result').html(res);
      });
      $('div#result').show();
    } else {
      $('div#result').hide();
    }
  });

});
