# README #

* Mobile Application based on [truckrepair.com](http://truckrepair.com)

![Untitled drawing.png](https://bitbucket.org/repo/KLLy55/images/2807117257-Untitled%20drawing.png)

### How to make it run ###

* npm install
* npm install gulp -g
* gulp (to run the application)
* or
* gulp watch to set-up a dev
* go to http://localhost:8888

**Due to that the truckrepair.com MySQL database is being used and credentials are not posted (for obvious reasons) you actually can't run it yourself, for a live (beta view) go to [truckrepair.com:8888](http://truckrepair.com:8888)

### Who do I talk to? ###

* willem.goudsbloem(at)gmail.com